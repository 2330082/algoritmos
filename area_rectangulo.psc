Proceso area_rectangulo
	Definir b, h, a Como Real;
	Escribir "Ingrese el valor de la base";
	Leer b;
	Escribir "Ingrese el valor de la altura";
	Leer h;
	a <- b * h;
	Si b > 0 & h > 0 & b <> h Entonces
		Escribir "El area del rectangulo es ", a, " u^2";
	SiNo 
		Escribir "Ingrese valores validos e intente de nuevo";
	FinSi
FinProceso
