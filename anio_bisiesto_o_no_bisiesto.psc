Proceso anio_bisiesto_o_no_bisiesto
	Definir anio Como Entero;
	Leer anio;
	Si anio % 400==0 Entonces
		Escribir "Es a�o bisiesto";
	SiNo
		Si (anio %4==0) && (!(anio %100==0)) Entonces
			Escribir "Es a�o bisiesto";
		SiNo 
			Escribir "No es a�o bisiesto";
		FinSi
	FinSi
FinProceso
