Proceso neveria
	Definir num, total Como Entero;
	Escribir "1) Fresa";
	Escribir "2) Vainilla";
	Escribir "3) Chocolate";
	Escribir "4) Napolitano";
	Escribir "5) Galleta";
	Escribir "6) Nuez";
	Escribir "Teclea el numero que desees";
	Leer num;
	Segun num Hacer
		1:
			total<-30;
			Escribir "El precio a pagar por el helado es: $", total;
		2: 
			total<-40;
			Escribir "El precio a pagar por el helado es: $", total;
		3:
			total<-45;
			Escribir "El precio a pagar por el helado es: $", total;
		4:
			total<-25;
			Escribir "El precio a pagar por el helado es: $", total;
		5:
			total<-50;
			Escribir "El precio a pagar por el helado es: $", total;
		6:
			total<-35;
			Escribir "El precio a pagar por el helado es: $", total;
		De Otro Modo:
			Escribir "Ese numero no se encuentra en el menu, intente otro";
	FinSegun
FinProceso
