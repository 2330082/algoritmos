Proceso saber_si_eres_mayor_de_edad
	Definir edad Como Entero;
	Escribir "Introducir edad";
	Leer edad;
	Si edad<1 o edad>130 Entonces
		Escribir "No te creo";
	SiNo
		Si edad>=18 Entonces
			Escribir "Eres mayor de edad";
		SiNo 
			Escribir "Eres menor de edad";
		FinSi
	FinSi
FinProceso
