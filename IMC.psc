Proceso Determinar_si_tienes_sobrepeso_dependiendo_de_tu_IMC
	Definir peso, altura, resultado Como Real;
	Escribir "�Cuanto pesas?";
	Leer peso;
	Escribir "�Cuanto mides?";
	Leer altura;
	resultado<-(peso/(altura*altura));
	Escribir "Tu IMC es: ", resultado;
	Definir num Como Entero;
	Escribir "Segun tu IMC, elige la opcion que te corresponda";
	Escribir "1) IMC de 18.5 a 24.9";
	Escribir "2) IMC de 25 a 29.9";
	Escribir "3) IMC de 30 a 34.9";
	Escribir "4) IMC de 35 a 39.9";
	Escribir "5) IMC mayor de 40";
	Leer num;
	Segun num Hacer
		1:
			Escribir "Tu peso es normal y tu riesgo es promedio";
		2: 
			Escribir "Tu tienes sobrepeso y tu riesgo es aumentado";
		3: 
			Escribir "Tu tienes obesidad grado 1 y tu riesgo es moderado";
		4: 
			Escribir "Tu tienes obesidad grado 2 y tu riesgo es severo";
		5:
			Escribir "Tu tienes obesidad grado 3 y tu riesgo es muy severo";
		De Otro Modo:
			Escribir "Ese numero no se encuentra en las opciones, elige otra opcion";
	FinSegun
	Escribir "Si tus resultados fueron graves, deberias pedir ayuda";
	
FinProceso
