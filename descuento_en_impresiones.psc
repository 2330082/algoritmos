Proceso descuento_en_impresiones
	Definir cantpag Como Entero;
	Definir costpag, total Como Real;
	
	Escribir "Ingrese la cantidad de paginas a imprimir";
	Leer cantpag;
	Escribir "Ingrese el costo de una pagina";
	Leer costpag;
	
	total<-cantpag*costpag;
	
	Si cantpag>29 Entonces
		total<-total*.80;
		Escribir "El precio a pagar es de: $", total;
	SiNo
		Escribir "El precio a pagar es de: $", total;
	FinSi
FinProceso
