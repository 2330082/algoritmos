Proceso nieveDescuento
	Definir nieves Como Real;
	Definir precioNieve Como Real;
	
	precioNieve<-20;
	
	Escribir ('Teclea la cantidad de nieves');
	Leer nieves;
	
	precioNieve<-precioNieve*nieves;
	
	Si (nieves>10) Entonces
		precioNieve <- precioNieve*0.6;
	SiNo
		precioNieve <- precioNieve*0.7;
	FinSi
	
	Escribir ('El total a pagar por las nieves es de: $'), precioNieve;
FinProceso
