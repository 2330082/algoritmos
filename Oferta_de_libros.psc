Proceso Oferta_de_libros
	Definir x, paquete, monto_pagar Como Real;
	Escribir "La libreria Blue Bird tiene en ofertas varios paquetes de libros";
	Escribir "Paquete 1: 5 libros, su costo original es de $1000 y tiene un descuento del 30%";
	Escribir "Paquete 2: 6 libros, su costo original es de $1500 y tiene un descuento del 35%";
	Escribir "Paquete 3: 7 libros, su costo original es de $2000 y tiene un descuento del 40%";
	Escribir "Eliga una opcion";
	Leer paquete;
	Si paquete==1 Entonces
		Escribir "Ha elegido el paquete 1";
		x<-(1000*30)/100;
		Escribir "El pago original era $1000, ya con el descuento son $", x;
		Escribir "Ingrese el monto a pagar";
		Leer monto_pagar;
		Si monto_pagar>=x Entonces
			Escribir "Gracias por su compra, su cambio es $", monto_pagar-(x);
		SiNo
			Escribir "El dinero no esta completo";
		FinSi
	FinSi
	Si paquete==2 Entonces
		Escribir "Ha elegido el paquete 2";
		x<-(1500*35)/100;
		Escribir "El pago original era $1500, ya con el descuento son $", x;
		Escribir "Ingrese el monto a pagar";
		Leer monto_pagar;
		Si monto_pagar>=x Entonces
			Escribir "Gracias por su compra, su cambio es $", monto_pagar-(x);
		SiNo
			Escribir "El dinero no esta completo";
		FinSi
	FinSi
	Si paquete==3 Entonces
		Escribir "Ha elegido el paquete 3";
		x<-(2000*40)/100;
		Escribir "El pago original era $2000, ya con el descuento son $", x;
		Escribir "Ingrese el monto a pagar";
		Leer monto_pagar;
		Si monto_pagar>=x Entonces
			Escribir "Gracias por su compra, su cambio es $", monto_pagar-x;
		SiNo
			Escribir "El dinero no esta completo";
		FinSi
	FinSi
	Escribir "Muchas gracias por visitarnos";
FinProceso
